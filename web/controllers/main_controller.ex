defmodule RajiniBot.MainController do
  use RajiniBot.Web, :controller
  alias RajiniBot.TelegramApi
  alias RajiniBot.ReminderGenserver

  @token Application.get_env(:rajini_bot, RajiniBot.Endpoint, :token) |> Keyword.get(:token)

  def index(conn, %{"update_id" => _update_id, "message" => message}) do
    message_id = message |> Map.get("message_id")
    chat_id = message |> Map.get("chat") |> Map.get("id")
    message
    |> Map.get("text")
    |> process_text(chat_id, message_id)
    json conn, %{ok: true}
  end

  defp process_text("/remind" <> text, chat_id, message_id) do
    text
    |> String.split
    |> process_reminder_text(chat_id, message_id)
  end

  defp process_reminder_text([], chat_id, message_id) do
    _ = TelegramApi.send_message(chat_id, "Wrong Format Idiot. Enter /help to see how to type it.", message_id)
  end
  defp process_reminder_text([time | message], chat_id, message_id) do
    time_int = String.to_integer(time)
    reminder = Enum.join(message, " ")
    :ok = ReminderGenserver.set_reminder(:reminderbot, time_int, reminder, chat_id, message_id)
  end
end
