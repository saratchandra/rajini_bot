defmodule RajiniBot.Router do
  use RajiniBot.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/rajini_bot", RajiniBot do
    pipe_through :api

    post "/", MainController, :index
  end
end
