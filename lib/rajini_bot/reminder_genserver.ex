defmodule RajiniBot.ReminderGenserver do
  use GenServer
  alias RajiniBot.TelegramApi

  def start_link(name) do
    GenServer.start_link(__MODULE__, :ok, name: name)
  end

  def set_reminder(server, time, message, chat_id, message_id) do
    GenServer.call(server, {:remind, time, message, chat_id, message_id})
  end

  def init(:ok) do
    {:ok, %{}}
  end

  def handle_call({:remind, time, message, chat_id, message_id}, _from, state) do
    Process.send_after(self, {:remind, message, chat_id, message_id}, time * 60000)
    {:reply, :ok, state}
  end

  def handle_info({:remind, message, chat_id, message_id}, state) do
    reminder = "Reminder for you: " <> message
    _ = TelegramApi.send_message(chat_id, reminder, message_id)
    {:noreply, state}
  end
end
