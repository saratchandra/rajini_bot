defmodule RajiniBot.TelegramApi do
  require Logger

  @base_url "https://api.telegram.org/bot"
  @token Application.get_env(:rajini_bot, RajiniBot.Endpoint, :bot_token) |> Keyword.get(:bot_token)

  defp url do
    @base_url <> @token <> "/"
  end

  def send_message(chat_id, text, message_id, parse_mode \\ "Markdown") do
    post_url = "sendMessage"
    params = [{"chat_id", chat_id}, {"text", text}, {"parse_mode", parse_mode}, {"reply_to_message_id", message_id}]

    status = send_post(post_url, params)
              |> Map.fetch!("ok")
    case status do
      true ->
        :ok
      _ ->
        :error
    end
  end

  defp send_post(post_url, params) do
    Task.async(fn ->
      HTTPoison.request(:post, url() <> post_url, {:form, params})
    end)
    |> Task.await()
    |> get_resp()
    |> Map.fetch!(:body)
    |> Poison.decode!
  end

  def get_resp({:ok, resp}) do
    resp
  end
end
