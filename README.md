# RajiniBot

##### WIP

This is a Telegram Bot. This is planned to support a lot of actions from inside Telegram.

###  Planned actions
- [ ] Keep reminders for yourself
- [ ] Indian Railways PNR Status
- [ ] Weather Updates
- [ ] Sports Updates

### Development

This is totally written in Elixir/Phoenix. The documentation is a work in progress.
